#include "remote.hpp"
#include <fcntl.h>
#include <sys/ptrace.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

const unsigned long addrOfFive = 0x601040;
void* textPointer;
char name[64];


void printText(const char* text) {
    int fd = open(name, O_WRONLY);
    lseek(fd, (unsigned long)textPointer, SEEK_SET);
    write(fd, text, strlen(text));
}

void setResult(const char* res) {
    int fd = open(name, O_WRONLY);
    lseek(fd, addrOfFive, SEEK_SET);
    write(fd, res, strlen(res));
}

int main() {
    if(getuid()) {
        printf("You shall not pass!\n");
        return 0;
    }

    pid_t orig_pid;

    while(true) {
        printf("Waiting for original process..\n");
        if(remote::FindProcessByName("./orig") != -1){
            orig_pid = remote::FindProcessByName("./orig");
            break;
        }
        usleep(1000);
    }
    remote::Handle::pid = orig_pid;
    printf("pid: %d\n", orig_pid);
    
    textPointer = remote::Handle::FindPattern(
            "\x35\x20\x2b\x20\x35",
            "xxxxx");
    printf("textPointer: %x\n", textPointer);
    char *test = (char*)malloc(10);
    char sum[2];

    

    printf("text: %x\nfive: %x\n", textPointer, addrOfFive);
    
    
    sprintf(name, "/proc/%d/mem", orig_pid);
    char action = 0;
    for(;;) {
        printf("Avaliable actions:\n\t1.Change text\n\t2.Change summary arg\n");
        action = getchar();
	getchar();
        switch (action){
        case '2':
          printf("Enter argument: ");
          sum[0] = getchar();
          setResult(sum);
          action = 0;
          break;
        case '1':
          printf("Enter text: ");
          scanf("%s", test);
          printText(test);
          action = 0;
        }
        printf("Patched!:)");
        getchar();
    }


    return 0;
}
