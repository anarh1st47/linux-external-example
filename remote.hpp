#pragma once

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <limits.h>
#include <sys/uio.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>

namespace remote {
    namespace Handle {
        char* GetSymbolicLinkTarget(char* target);

        extern pid_t pid;
        extern char* pidStr;

        //Handle() : pid(-1) {}
        void init(pid_t target);
        void init(char* target);

        char* GetPath();
        char* GetWorkingDirectory();
        //pid_t GetPid(){ return pid; }

        void ParseMaps();

        bool IsValid();

	void *FindPattern(const char* data, const char* pattern);
	bool Read(void* address, void* buffer, size_t size);
	bool Write(void* address, void* buffer, size_t size);

        unsigned long GetCallAddress(void* address);

    };

    pid_t FindProcessByName(const char* name);
};
