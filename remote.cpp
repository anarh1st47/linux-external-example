#include "remote.hpp"
#include <stdlib.h>
#include <string.h>

#define FINDPATTERN_CHUNKSIZE 0x1000

pid_t remote::Handle::pid;
char* remote::Handle::pidStr;

void reverse(char s[])
 {
     int i, j;
     char c;
 
     for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
         c = s[i];
         s[i] = s[j];
         s[j] = c;
     }
 }

 void itoa(int n, char s[]) {
     int i, sign;
 
     if ((sign = n) < 0)  /* записываем знак */
         n = -n;          /* делаем n положительным числом */
     i = 0;
     do {       /* генерируем цифры в обратном порядке */
         s[i++] = n % 10 + '0';   /* берем следующую цифру */
     } while ((n /= 10) > 0);     /* удаляем */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
 }

namespace remote {
    // Handle
    void Handle::init(pid_t target) {
        char buffer[10];
        pid = target;
        itoa(target, pidStr);
    }

    void Handle::init(char* target) {
        // Check to see if the string is numeric (no negatives or dec allowed, which makes this function usable)
        if(strspn(target, "0123456789") != sizeof(target)) {
            pid = -1;
            pidStr = 0;
        } else {
            pidStr = target;
            pid = -1;
        }
    }

	void *Handle::FindPattern(const char* data, const char* pattern) {
		char buffer[0x1000];

		size_t len = strlen(pattern);
		size_t chunksize = sizeof(buffer);
		size_t totalsize = 0x5000;
		size_t chunknum = 0;

		while (totalsize) {
			size_t readsize = (totalsize < chunksize) ? totalsize : chunksize;
			size_t readaddr = chunksize * chunknum;

			bzero(buffer, chunksize);

			if (Read((void*)readaddr, buffer, readsize)) {
				for (size_t b = 0; b < readsize; b++) {
					size_t matches = 0;

					while (buffer[b + matches] == data[matches] || pattern[matches] != 'x') {
						matches++;

						if (matches == len)
							return (char*)(readaddr + b);
					}
				}
			}

			totalsize--;
			chunknum++;
		}
		printf("note: NULL pattern\n");
		return NULL;
	}


    bool Handle::IsValid() {
        return (pid != -1);
    }

    bool Handle::Write(void* address, void* buffer, size_t size) {
        struct iovec local[1];
        struct iovec remote[1];

        local[0].iov_base = buffer;
        local[0].iov_len = size;
        remote[0].iov_base = address;
        remote[0].iov_len = size;

        return (process_vm_writev(pid, local, 1, remote, 1, 0) == size);
    }

    bool Handle::Read(void* address, void* buffer, size_t size) {
        struct iovec local[1];
        struct iovec remote[1];

        local[0].iov_base = buffer;
        local[0].iov_len = size;
        remote[0].iov_base = address;
        remote[0].iov_len = size;

        return (process_vm_readv(pid, local, 1, remote, 1, 0) == size);
    }

    unsigned long Handle::GetCallAddress(void* address) {
        unsigned long code = 0;

        if(Read((char*) address + 1, &code, sizeof(unsigned long))) {
            return code + (unsigned long) address + 5;
        }

        return 0;
    }


    char* Handle::GetSymbolicLinkTarget(char* target) {
        char buf[PATH_MAX];

        ssize_t len = ::readlink(target, buf, sizeof(buf) - 1);

        if(len != -1) {
            buf[len] = 0;

            return buf;
        }

        return "";
    }
pid_t FindProcessByName(const char* name) {
    DIR* dir;
    struct dirent* ent;
    char* endptr;
    char buf[512];
    
    if (!(dir = opendir("/proc"))) {
        perror("can't open /proc");
        return -1;
    }

    while((ent = readdir(dir)) != NULL) {
        /* if endptr is not a null character, the directory is not
         * entirely numeric, so ignore it */
        long lpid = strtol(ent->d_name, &endptr, 10);
        if (*endptr != '\0') {
            continue;
        }

        /* try to open the cmdline file */
        snprintf(buf, sizeof(buf), "/proc/%ld/cmdline", lpid);
        FILE* fp = fopen(buf, "r");
        
        if (fp) {
            if (fgets(buf, sizeof(buf), fp) != NULL) {
                /* check the first token in the file, the program name */
                char* first = strtok(buf, " ");
                if (!strcmp(first, name)) {
                    fclose(fp);
                    closedir(dir);
                    return (pid_t)lpid;
                }
            }
            fclose(fp);
        }
        
    }
    
    closedir(dir);
    return -1;
}
};


