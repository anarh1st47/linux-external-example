#include <stdio.h>
#include <unistd.h>

int sum(int x, int y) {
	return x + y;
}

int main() {
    int a = 5;
    static int b = 5;
	while (true) {
		printf("5 + 5 = %d\n", sum(a, b));
		sleep(1); //secs
	}
}
